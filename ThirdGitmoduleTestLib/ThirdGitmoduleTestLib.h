//
//  ThirdGitmoduleTestLib.h
//  ThirdGitmoduleTestLib
//
//  Created by Julia Vashchenko on 10/09/2018.
//  Copyright © 2018 Julia Vashchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThirdGitmoduleTestLib : NSObject

- (instancetype) initWithInt:(int)anInt;
- (instancetype) initWithFloat:(float)aFloat;
@end
