//
//  ThirdGitmoduleTestLib.m
//  ThirdGitmoduleTestLib
//
//  Created by Julia Vashchenko on 10/09/2018.
//  Copyright © 2018 Julia Vashchenko. All rights reserved.
//

#import "ThirdGitmoduleTestLib.h"

@implementation ThirdGitmoduleTestLib

- (instancetype) initWithInt:(int)anInt {
    self = [super init];
    return self;
}

- (instancetype) initWithFloat:(float)aFloat {
    self = [super init];
    return self;
}

@end
